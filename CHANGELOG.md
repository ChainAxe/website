# Changelog

## 0.0.2

- Add basic UI
- Add models and deserialization
- Add Data loading

## 0.0.1

- Initial version, created by Stagehand
