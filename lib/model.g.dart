// GENERATED CODE - DO NOT MODIFY BY HAND

part of model;

// **************************************************************************
// Generator: BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_returning_this
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first

Serializer<Config> _$configSerializer = new _$ConfigSerializer();
Serializer<Algo> _$algoSerializer = new _$AlgoSerializer();
Serializer<Coin> _$coinSerializer = new _$CoinSerializer();
Serializer<Pool> _$poolSerializer = new _$PoolSerializer();
Serializer<BasicStats> _$basicStatsSerializer = new _$BasicStatsSerializer();
Serializer<PoolStats> _$poolStatsSerializer = new _$PoolStatsSerializer();
Serializer<Miner> _$minerSerializer = new _$MinerSerializer();

class _$ConfigSerializer implements StructuredSerializer<Config> {
  @override
  final Iterable<Type> types = const [Config, _$Config];
  @override
  final String wireName = 'Config';

  @override
  Iterable serialize(Serializers serializers, Config object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'algos',
      serializers.serialize(object.algos,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Algo)])),
      'coins',
      serializers.serialize(object.coins,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Coin)])),
      'pools',
      serializers.serialize(object.pools,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Pool)])),
    ];

    return result;
  }

  @override
  Config deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new ConfigBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'algos':
          result.algos.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Algo)]))
              as BuiltList);
          break;
        case 'coins':
          result.coins.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Coin)]))
              as BuiltList);
          break;
        case 'pools':
          result.pools.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Pool)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$AlgoSerializer implements StructuredSerializer<Algo> {
  @override
  final Iterable<Type> types = const [Algo, _$Algo];
  @override
  final String wireName = 'Algo';

  @override
  Iterable serialize(Serializers serializers, Algo object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'hashUnit',
      serializers.serialize(object.hashUnit,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Algo deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new AlgoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'hashUnit':
          result.hashUnit = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CoinSerializer implements StructuredSerializer<Coin> {
  @override
  final Iterable<Type> types = const [Coin, _$Coin];
  @override
  final String wireName = 'Coin';

  @override
  Iterable serialize(Serializers serializers, Coin object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'code',
      serializers.serialize(object.code, specifiedType: const FullType(String)),
      'algoid',
      serializers.serialize(object.algoid, specifiedType: const FullType(int)),
      'blocktime',
      serializers.serialize(object.blocktime,
          specifiedType: const FullType(double)),
      'explorer',
      serializers.serialize(object.explorer,
          specifiedType: const FullType(BuiltMap,
              const [const FullType(String), const FullType(String)])),
    ];

    return result;
  }

  @override
  Coin deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new CoinBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'algoid':
          result.algoid = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'blocktime':
          result.blocktime = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'explorer':
          result.explorer.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(String),
                const FullType(String)
              ])) as BuiltMap);
          break;
      }
    }

    return result.build();
  }
}

class _$PoolSerializer implements StructuredSerializer<Pool> {
  @override
  final Iterable<Type> types = const [Pool, _$Pool];
  @override
  final String wireName = 'Pool';

  @override
  Iterable serialize(Serializers serializers, Pool object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'coincode',
      serializers.serialize(object.coincode,
          specifiedType: const FullType(String)),
      'shortlocation',
      serializers.serialize(object.shortlocation,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Pool deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new PoolBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'coincode':
          result.coincode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'shortlocation':
          result.shortlocation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$BasicStatsSerializer implements StructuredSerializer<BasicStats> {
  @override
  final Iterable<Type> types = const [BasicStats, _$BasicStats];
  @override
  final String wireName = 'BasicStats';

  @override
  Iterable serialize(Serializers serializers, BasicStats object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'poolstats',
      serializers.serialize(object.poolstats,
          specifiedType:
              const FullType(BuiltList, const [const FullType(PoolStats)])),
    ];

    return result;
  }

  @override
  BasicStats deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new BasicStatsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'poolstats':
          result.poolstats.replace(serializers.deserialize(value,
              specifiedType: const FullType(
                  BuiltList, const [const FullType(PoolStats)])) as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$PoolStatsSerializer implements StructuredSerializer<PoolStats> {
  @override
  final Iterable<Type> types = const [PoolStats, _$PoolStats];
  @override
  final String wireName = 'PoolStats';

  @override
  Iterable serialize(Serializers serializers, PoolStats object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'hashrate',
      serializers.serialize(object.hashrate,
          specifiedType: const FullType(num)),
      'difficulty',
      serializers.serialize(object.difficulty,
          specifiedType: const FullType(int)),
      'blockHeight',
      serializers.serialize(object.blockHeight,
          specifiedType: const FullType(int)),
      'networkHashrate',
      serializers.serialize(object.networkHashrate,
          specifiedType: const FullType(int)),
    ];
    if (object.miners != null) {
      result
        ..add('miners')
        ..add(serializers.serialize(object.miners,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Miner)])));
    }

    return result;
  }

  @override
  PoolStats deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new PoolStatsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'hashrate':
          result.hashrate = serializers.deserialize(value,
              specifiedType: const FullType(num)) as num;
          break;
        case 'difficulty':
          result.difficulty = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'blockHeight':
          result.blockHeight = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'networkHashrate':
          result.networkHashrate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'miners':
          result.miners.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Miner)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$MinerSerializer implements StructuredSerializer<Miner> {
  @override
  final Iterable<Type> types = const [Miner, _$Miner];
  @override
  final String wireName = 'Miner';

  @override
  Iterable serialize(Serializers serializers, Miner object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'address',
      serializers.serialize(object.address,
          specifiedType: const FullType(String)),
      'lastShare',
      serializers.serialize(object.lastShare,
          specifiedType: const FullType(String)),
    ];
    if (object.hashrate != null) {
      result
        ..add('hashrate')
        ..add(serializers.serialize(object.hashrate,
            specifiedType: const FullType(int)));
    }

    return result;
  }

  @override
  Miner deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new MinerBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'address':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastShare':
          result.lastShare = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'hashrate':
          result.hashrate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$Config extends Config {
  @override
  final BuiltList<Algo> algos;
  @override
  final BuiltList<Coin> coins;
  @override
  final BuiltList<Pool> pools;

  factory _$Config([void updates(ConfigBuilder b)]) =>
      (new ConfigBuilder()..update(updates)).build();

  _$Config._({this.algos, this.coins, this.pools}) : super._() {
    if (algos == null) throw new BuiltValueNullFieldError('Config', 'algos');
    if (coins == null) throw new BuiltValueNullFieldError('Config', 'coins');
    if (pools == null) throw new BuiltValueNullFieldError('Config', 'pools');
  }

  @override
  Config rebuild(void updates(ConfigBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ConfigBuilder toBuilder() => new ConfigBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Config) return false;
    return algos == other.algos && coins == other.coins && pools == other.pools;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, algos.hashCode), coins.hashCode), pools.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Config')
          ..add('algos', algos)
          ..add('coins', coins)
          ..add('pools', pools))
        .toString();
  }
}

class ConfigBuilder implements Builder<Config, ConfigBuilder> {
  _$Config _$v;

  ListBuilder<Algo> _algos;
  ListBuilder<Algo> get algos => _$this._algos ??= new ListBuilder<Algo>();
  set algos(ListBuilder<Algo> algos) => _$this._algos = algos;

  ListBuilder<Coin> _coins;
  ListBuilder<Coin> get coins => _$this._coins ??= new ListBuilder<Coin>();
  set coins(ListBuilder<Coin> coins) => _$this._coins = coins;

  ListBuilder<Pool> _pools;
  ListBuilder<Pool> get pools => _$this._pools ??= new ListBuilder<Pool>();
  set pools(ListBuilder<Pool> pools) => _$this._pools = pools;

  ConfigBuilder();

  ConfigBuilder get _$this {
    if (_$v != null) {
      _algos = _$v.algos?.toBuilder();
      _coins = _$v.coins?.toBuilder();
      _pools = _$v.pools?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Config other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Config;
  }

  @override
  void update(void updates(ConfigBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Config build() {
    _$Config _$result;
    try {
      _$result = _$v ??
          new _$Config._(
              algos: algos.build(), coins: coins.build(), pools: pools.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'algos';
        algos.build();
        _$failedField = 'coins';
        coins.build();
        _$failedField = 'pools';
        pools.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Config', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Algo extends Algo {
  @override
  final int id;
  @override
  final String name;
  @override
  final String hashUnit;

  factory _$Algo([void updates(AlgoBuilder b)]) =>
      (new AlgoBuilder()..update(updates)).build();

  _$Algo._({this.id, this.name, this.hashUnit}) : super._() {
    if (id == null) throw new BuiltValueNullFieldError('Algo', 'id');
    if (name == null) throw new BuiltValueNullFieldError('Algo', 'name');
    if (hashUnit == null)
      throw new BuiltValueNullFieldError('Algo', 'hashUnit');
  }

  @override
  Algo rebuild(void updates(AlgoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AlgoBuilder toBuilder() => new AlgoBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Algo) return false;
    return id == other.id && name == other.name && hashUnit == other.hashUnit;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), name.hashCode), hashUnit.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Algo')
          ..add('id', id)
          ..add('name', name)
          ..add('hashUnit', hashUnit))
        .toString();
  }
}

class AlgoBuilder implements Builder<Algo, AlgoBuilder> {
  _$Algo _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _hashUnit;
  String get hashUnit => _$this._hashUnit;
  set hashUnit(String hashUnit) => _$this._hashUnit = hashUnit;

  AlgoBuilder();

  AlgoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _hashUnit = _$v.hashUnit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Algo other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Algo;
  }

  @override
  void update(void updates(AlgoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Algo build() {
    final _$result =
        _$v ?? new _$Algo._(id: id, name: name, hashUnit: hashUnit);
    replace(_$result);
    return _$result;
  }
}

class _$Coin extends Coin {
  @override
  final String name;
  @override
  final String code;
  @override
  final int algoid;
  @override
  final double blocktime;
  @override
  final BuiltMap<String, String> explorer;

  factory _$Coin([void updates(CoinBuilder b)]) =>
      (new CoinBuilder()..update(updates)).build();

  _$Coin._({this.name, this.code, this.algoid, this.blocktime, this.explorer})
      : super._() {
    if (name == null) throw new BuiltValueNullFieldError('Coin', 'name');
    if (code == null) throw new BuiltValueNullFieldError('Coin', 'code');
    if (algoid == null) throw new BuiltValueNullFieldError('Coin', 'algoid');
    if (blocktime == null)
      throw new BuiltValueNullFieldError('Coin', 'blocktime');
    if (explorer == null)
      throw new BuiltValueNullFieldError('Coin', 'explorer');
  }

  @override
  Coin rebuild(void updates(CoinBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  CoinBuilder toBuilder() => new CoinBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Coin) return false;
    return name == other.name &&
        code == other.code &&
        algoid == other.algoid &&
        blocktime == other.blocktime &&
        explorer == other.explorer;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, name.hashCode), code.hashCode), algoid.hashCode),
            blocktime.hashCode),
        explorer.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Coin')
          ..add('name', name)
          ..add('code', code)
          ..add('algoid', algoid)
          ..add('blocktime', blocktime)
          ..add('explorer', explorer))
        .toString();
  }
}

class CoinBuilder implements Builder<Coin, CoinBuilder> {
  _$Coin _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _code;
  String get code => _$this._code;
  set code(String code) => _$this._code = code;

  int _algoid;
  int get algoid => _$this._algoid;
  set algoid(int algoid) => _$this._algoid = algoid;

  double _blocktime;
  double get blocktime => _$this._blocktime;
  set blocktime(double blocktime) => _$this._blocktime = blocktime;

  MapBuilder<String, String> _explorer;
  MapBuilder<String, String> get explorer =>
      _$this._explorer ??= new MapBuilder<String, String>();
  set explorer(MapBuilder<String, String> explorer) =>
      _$this._explorer = explorer;

  CoinBuilder();

  CoinBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _code = _$v.code;
      _algoid = _$v.algoid;
      _blocktime = _$v.blocktime;
      _explorer = _$v.explorer?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Coin other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Coin;
  }

  @override
  void update(void updates(CoinBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Coin build() {
    _$Coin _$result;
    try {
      _$result = _$v ??
          new _$Coin._(
              name: name,
              code: code,
              algoid: algoid,
              blocktime: blocktime,
              explorer: explorer.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'explorer';
        explorer.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Coin', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Pool extends Pool {
  @override
  final int id;
  @override
  final String coincode;
  @override
  final String shortlocation;

  factory _$Pool([void updates(PoolBuilder b)]) =>
      (new PoolBuilder()..update(updates)).build();

  _$Pool._({this.id, this.coincode, this.shortlocation}) : super._() {
    if (id == null) throw new BuiltValueNullFieldError('Pool', 'id');
    if (coincode == null)
      throw new BuiltValueNullFieldError('Pool', 'coincode');
    if (shortlocation == null)
      throw new BuiltValueNullFieldError('Pool', 'shortlocation');
  }

  @override
  Pool rebuild(void updates(PoolBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PoolBuilder toBuilder() => new PoolBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Pool) return false;
    return id == other.id &&
        coincode == other.coincode &&
        shortlocation == other.shortlocation;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, id.hashCode), coincode.hashCode), shortlocation.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Pool')
          ..add('id', id)
          ..add('coincode', coincode)
          ..add('shortlocation', shortlocation))
        .toString();
  }
}

class PoolBuilder implements Builder<Pool, PoolBuilder> {
  _$Pool _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _coincode;
  String get coincode => _$this._coincode;
  set coincode(String coincode) => _$this._coincode = coincode;

  String _shortlocation;
  String get shortlocation => _$this._shortlocation;
  set shortlocation(String shortlocation) =>
      _$this._shortlocation = shortlocation;

  PoolBuilder();

  PoolBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _coincode = _$v.coincode;
      _shortlocation = _$v.shortlocation;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Pool other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Pool;
  }

  @override
  void update(void updates(PoolBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Pool build() {
    final _$result = _$v ??
        new _$Pool._(id: id, coincode: coincode, shortlocation: shortlocation);
    replace(_$result);
    return _$result;
  }
}

class _$BasicStats extends BasicStats {
  @override
  final BuiltList<PoolStats> poolstats;

  factory _$BasicStats([void updates(BasicStatsBuilder b)]) =>
      (new BasicStatsBuilder()..update(updates)).build();

  _$BasicStats._({this.poolstats}) : super._() {
    if (poolstats == null)
      throw new BuiltValueNullFieldError('BasicStats', 'poolstats');
  }

  @override
  BasicStats rebuild(void updates(BasicStatsBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  BasicStatsBuilder toBuilder() => new BasicStatsBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! BasicStats) return false;
    return poolstats == other.poolstats;
  }

  @override
  int get hashCode {
    return $jf($jc(0, poolstats.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BasicStats')
          ..add('poolstats', poolstats))
        .toString();
  }
}

class BasicStatsBuilder implements Builder<BasicStats, BasicStatsBuilder> {
  _$BasicStats _$v;

  ListBuilder<PoolStats> _poolstats;
  ListBuilder<PoolStats> get poolstats =>
      _$this._poolstats ??= new ListBuilder<PoolStats>();
  set poolstats(ListBuilder<PoolStats> poolstats) =>
      _$this._poolstats = poolstats;

  BasicStatsBuilder();

  BasicStatsBuilder get _$this {
    if (_$v != null) {
      _poolstats = _$v.poolstats?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BasicStats other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$BasicStats;
  }

  @override
  void update(void updates(BasicStatsBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$BasicStats build() {
    _$BasicStats _$result;
    try {
      _$result = _$v ?? new _$BasicStats._(poolstats: poolstats.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'poolstats';
        poolstats.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BasicStats', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$PoolStats extends PoolStats {
  @override
  final int id;
  @override
  final num hashrate;
  @override
  final int difficulty;
  @override
  final int blockHeight;
  @override
  final int networkHashrate;
  @override
  final BuiltList<Miner> miners;

  factory _$PoolStats([void updates(PoolStatsBuilder b)]) =>
      (new PoolStatsBuilder()..update(updates)).build();

  _$PoolStats._(
      {this.id,
      this.hashrate,
      this.difficulty,
      this.blockHeight,
      this.networkHashrate,
      this.miners})
      : super._() {
    if (id == null) throw new BuiltValueNullFieldError('PoolStats', 'id');
    if (hashrate == null)
      throw new BuiltValueNullFieldError('PoolStats', 'hashrate');
    if (difficulty == null)
      throw new BuiltValueNullFieldError('PoolStats', 'difficulty');
    if (blockHeight == null)
      throw new BuiltValueNullFieldError('PoolStats', 'blockHeight');
    if (networkHashrate == null)
      throw new BuiltValueNullFieldError('PoolStats', 'networkHashrate');
  }

  @override
  PoolStats rebuild(void updates(PoolStatsBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PoolStatsBuilder toBuilder() => new PoolStatsBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! PoolStats) return false;
    return id == other.id &&
        hashrate == other.hashrate &&
        difficulty == other.difficulty &&
        blockHeight == other.blockHeight &&
        networkHashrate == other.networkHashrate &&
        miners == other.miners;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), hashrate.hashCode),
                    difficulty.hashCode),
                blockHeight.hashCode),
            networkHashrate.hashCode),
        miners.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PoolStats')
          ..add('id', id)
          ..add('hashrate', hashrate)
          ..add('difficulty', difficulty)
          ..add('blockHeight', blockHeight)
          ..add('networkHashrate', networkHashrate)
          ..add('miners', miners))
        .toString();
  }
}

class PoolStatsBuilder implements Builder<PoolStats, PoolStatsBuilder> {
  _$PoolStats _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  num _hashrate;
  num get hashrate => _$this._hashrate;
  set hashrate(num hashrate) => _$this._hashrate = hashrate;

  int _difficulty;
  int get difficulty => _$this._difficulty;
  set difficulty(int difficulty) => _$this._difficulty = difficulty;

  int _blockHeight;
  int get blockHeight => _$this._blockHeight;
  set blockHeight(int blockHeight) => _$this._blockHeight = blockHeight;

  int _networkHashrate;
  int get networkHashrate => _$this._networkHashrate;
  set networkHashrate(int networkHashrate) =>
      _$this._networkHashrate = networkHashrate;

  ListBuilder<Miner> _miners;
  ListBuilder<Miner> get miners => _$this._miners ??= new ListBuilder<Miner>();
  set miners(ListBuilder<Miner> miners) => _$this._miners = miners;

  PoolStatsBuilder();

  PoolStatsBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _hashrate = _$v.hashrate;
      _difficulty = _$v.difficulty;
      _blockHeight = _$v.blockHeight;
      _networkHashrate = _$v.networkHashrate;
      _miners = _$v.miners?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PoolStats other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$PoolStats;
  }

  @override
  void update(void updates(PoolStatsBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PoolStats build() {
    _$PoolStats _$result;
    try {
      _$result = _$v ??
          new _$PoolStats._(
              id: id,
              hashrate: hashrate,
              difficulty: difficulty,
              blockHeight: blockHeight,
              networkHashrate: networkHashrate,
              miners: _miners?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'miners';
        _miners?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PoolStats', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Miner extends Miner {
  @override
  final String address;
  @override
  final String lastShare;
  @override
  final int hashrate;

  factory _$Miner([void updates(MinerBuilder b)]) =>
      (new MinerBuilder()..update(updates)).build();

  _$Miner._({this.address, this.lastShare, this.hashrate}) : super._() {
    if (address == null) throw new BuiltValueNullFieldError('Miner', 'address');
    if (lastShare == null)
      throw new BuiltValueNullFieldError('Miner', 'lastShare');
  }

  @override
  Miner rebuild(void updates(MinerBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MinerBuilder toBuilder() => new MinerBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Miner) return false;
    return address == other.address &&
        lastShare == other.lastShare &&
        hashrate == other.hashrate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, address.hashCode), lastShare.hashCode), hashrate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Miner')
          ..add('address', address)
          ..add('lastShare', lastShare)
          ..add('hashrate', hashrate))
        .toString();
  }
}

class MinerBuilder implements Builder<Miner, MinerBuilder> {
  _$Miner _$v;

  String _address;
  String get address => _$this._address;
  set address(String address) => _$this._address = address;

  String _lastShare;
  String get lastShare => _$this._lastShare;
  set lastShare(String lastShare) => _$this._lastShare = lastShare;

  int _hashrate;
  int get hashrate => _$this._hashrate;
  set hashrate(int hashrate) => _$this._hashrate = hashrate;

  MinerBuilder();

  MinerBuilder get _$this {
    if (_$v != null) {
      _address = _$v.address;
      _lastShare = _$v.lastShare;
      _hashrate = _$v.hashrate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Miner other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Miner;
  }

  @override
  void update(void updates(MinerBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Miner build() {
    final _$result = _$v ??
        new _$Miner._(
            address: address, lastShare: lastShare, hashrate: hashrate);
    replace(_$result);
    return _$result;
  }
}
