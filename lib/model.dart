library model;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'model.g.dart';

abstract class Config implements Built<Config, ConfigBuilder> {
  BuiltList<Algo> get algos;

  BuiltList<Coin> get coins;

  BuiltList<Pool> get pools;

  Algo getAlgoById(int i) => algos.firstWhere((Algo a) => a.id == i);

  Coin getCoinByCode(String code) =>
      coins.firstWhere((Coin c) => c.code == code);

  Pool getPoolById(int i) => pools.firstWhere((Pool p) => p.id == i);

  Config._();

  factory Config([updates(ConfigBuilder b)]) = _$Config;

  static Serializer<Config> get serializer => _$configSerializer;
}

abstract class Algo implements Built<Algo, AlgoBuilder> {
  int get id;

  String get name;

  String get hashUnit;

  Algo._();

  factory Algo([updates(AlgoBuilder b)]) = _$Algo;

  static Serializer<Algo> get serializer => _$algoSerializer;
}

abstract class Coin implements Built<Coin, CoinBuilder> {
  String get name;

  String get code;

  int get algoid;

  double get blocktime;

  BuiltMap<String, String> get explorer;

  Coin._();

  factory Coin([updates(CoinBuilder b)]) = _$Coin;

  static Serializer<Coin> get serializer => _$coinSerializer;
}

abstract class Pool implements Built<Pool, PoolBuilder> {
  int get id;

  String get coincode;

  String get shortlocation;

  //String get url;

  Pool._();

  factory Pool([updates(PoolBuilder b)]) = _$Pool;

  static Serializer<Pool> get serializer => _$poolSerializer;
}

abstract class BasicStats implements Built<BasicStats, BasicStatsBuilder> {
  BuiltList<PoolStats> get poolstats;

  PoolStats getPoolStatsByPoolId(int id) =>
      poolstats.firstWhere((PoolStats ps) => ps.id == id);

  BasicStats._();

  factory BasicStats([updates(BasicStatsBuilder b)]) = _$BasicStats;

  static Serializer<BasicStats> get serializer => _$basicStatsSerializer;
}

abstract class PoolStats implements Built<PoolStats, PoolStatsBuilder> {
  int get id;

  num get hashrate;

  int get difficulty;

  int get blockHeight;

  int get networkHashrate;

  @nullable
  BuiltList<Miner> get miners;

  List<Miner> getActiveMiners() {
    List<Miner> mList = new List<Miner>();
    if (null != miners) {
      mList = miners.where((miner) => miner.hashrate != null && miner.hashrate > 0).toList();
      mList.sort((a,b) => b.hashrate.compareTo(a.hashrate));
    }
    return mList;

  }

  int getActiveMinersCount() {
    int minerCount = 0;
    if (null != miners) {
      minerCount = getActiveMiners().length;
    }
    return minerCount;
  }

  PoolStats._();

  factory PoolStats([updates(PoolStatsBuilder b)]) = _$PoolStats;

  static Serializer<PoolStats> get serializer => _$poolStatsSerializer;
}

abstract class Miner implements Built<Miner, MinerBuilder> {
  String get address;

  String get lastShare; //ISO8601 String
  @nullable
  int get hashrate;

  Miner._();

  factory Miner([updates(MinerBuilder b)]) = _$Miner;

  static Serializer<Miner> get serializer => _$minerSerializer;
}
