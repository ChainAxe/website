import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:chainaxe_website/src/components/route_page_components/pool_id_route_page/pool_id_route_page.dart';
import 'package:chainaxe_website/src/components/route_page_components/pool_overview_route_page/pool_overview_route_page_component.dart';
import 'package:chainaxe_website/src/services/app_routes_service.dart';
import 'package:chainaxe_website/src/services/config_service.dart';
import 'package:chainaxe_website/src/services/stats_service.dart';

// AngularDart info: https://webdev.dartlang.org/angular
// Components info: https://webdev.dartlang.org/components

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [routerDirectives, PoolOverviewRoutePageComponent, PoolIdRoutePageComponent],
  providers: const [AppRoutesService, ConfigService, StatsService],
)
class AppComponent {
  final ConfigService _cs;
  final StatsService _ss;
  final AppRoutesService routes;

  AppComponent(this._cs, this._ss, this.routes);

  String currentYear = "${new DateTime.now().year}";
  bool isActive = false;

  void toggleNavbar() {
    isActive = !isActive;
    print("toggle ${isActive ? "true" : "false"}");
  }
}
