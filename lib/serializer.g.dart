// GENERATED CODE - DO NOT MODIFY BY HAND

part of serializers;

// **************************************************************************
// Generator: BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_returning_this
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(Algo.serializer)
      ..add(BasicStats.serializer)
      ..add(Coin.serializer)
      ..add(Config.serializer)
      ..add(Miner.serializer)
      ..add(Pool.serializer)
      ..add(PoolStats.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Algo)]),
          () => new ListBuilder<Algo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Coin)]),
          () => new ListBuilder<Coin>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Pool)]),
          () => new ListBuilder<Pool>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Miner)]),
          () => new ListBuilder<Miner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PoolStats)]),
          () => new ListBuilder<PoolStats>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>()))
    .build();
