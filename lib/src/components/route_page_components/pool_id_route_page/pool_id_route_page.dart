import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:chainaxe_website/model.dart';
import 'package:chainaxe_website/src/models/pool_group_model.dart';
import 'package:chainaxe_website/src/pipes/difficulty_pipe.dart';
import 'package:chainaxe_website/src/pipes/duration_pipe.dart';
import 'package:chainaxe_website/src/pipes/hashrate_pipe.dart';
import 'package:chainaxe_website/src/services/app_routes_service.dart';
import 'package:chainaxe_website/src/services/config_service.dart';
import 'package:chainaxe_website/src/services/stats_service.dart';

@Component(
    selector: 'pool-id-route-page',
    templateUrl: 'pool_id_route_page.html',
    directives: const [coreDirectives, routerDirectives],
    pipes: const [COMMON_PIPES, HashratePipe, DifficultyPipe, DurationPipe])
class PoolIdRoutePageComponent implements OnActivate {

  final ConfigService _cs;
  final StatsService _ss;
  final AppRoutesService routes;

  PoolGroupModel poolGroupModel;

  Map get urlParameters => {'id': poolGroupModel.pool.id};

  PoolIdRoutePageComponent(this._cs, this._ss, this.routes);

  @override
  Future onActivate(RouterState previous, RouterState current) async {
      final int id = int.parse(current.parameters['id']);
      Config config = await _cs.config;
      Pool p = config.pools.firstWhere((p) => p.id == id);
      Coin c = config.getCoinByCode(p.coincode);
      Algo a = config.getAlgoById(c.algoid);
      poolGroupModel = new PoolGroupModel(_ss, p, c, a);
  }

}