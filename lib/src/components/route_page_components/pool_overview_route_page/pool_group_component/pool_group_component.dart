import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:chainaxe_website/src/models/pool_group_model.dart';
import 'package:chainaxe_website/src/pipes/difficulty_pipe.dart';
import 'package:chainaxe_website/src/pipes/duration_pipe.dart';
import 'package:chainaxe_website/src/pipes/hashrate_pipe.dart';
import 'package:chainaxe_website/src/services/app_routes_service.dart';

@Component(
    selector: 'pool-group',
    templateUrl: 'pool_group_component.html',
    styles: ['.modal-background { cursor: pointer; }'],
    directives: const [coreDirectives, routerDirectives],
    pipes: const [COMMON_PIPES, HashratePipe, DifficultyPipe, DurationPipe])
class PoolGroupComponent {
  final AppRoutesService routes;
  bool minersModal = false;
  @Input() PoolGroupModel poolGroupModel;

  Map get urlParameters => {'id': poolGroupModel.pool.id};

  PoolGroupComponent(this.routes);
}
