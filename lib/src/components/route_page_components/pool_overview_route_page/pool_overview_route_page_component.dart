import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:chainaxe_website/model.dart';
import 'package:chainaxe_website/src/components/route_page_components/pool_overview_route_page/pool_group_component/pool_group_component.dart';
import 'package:chainaxe_website/src/models/pool_group_model.dart';
import 'package:chainaxe_website/src/pipes/difficulty_pipe.dart';
import 'package:chainaxe_website/src/pipes/duration_pipe.dart';
import 'package:chainaxe_website/src/pipes/hashrate_pipe.dart';
import 'package:chainaxe_website/src/services/app_routes_service.dart';
import 'package:chainaxe_website/src/services/config_service.dart';
import 'package:chainaxe_website/src/services/stats_service.dart';

@Component(
    selector: 'pool-overview-route-page',
    templateUrl: 'pool_overview_route_page_component.html',
    styleUrls: ['pool_overview_route_page_component.css'],
    directives: const [coreDirectives, PoolGroupComponent, routerDirectives],
    pipes: const [COMMON_PIPES, HashratePipe, DifficultyPipe, DurationPipe])
class PoolOverviewRoutePageComponent implements OnInit {
  final ConfigService _cs;
  final StatsService _ss;
  final AppRoutesService routes;

  bool isLoaded = false;

  Duration _lastUpdatedDuration;
  Duration get lastUpdated {
    if (null == _lastUpdatedDuration) _lastUpdatedDuration = getLastUpdatedDuration();
    return _lastUpdatedDuration;
  }

  List<PoolGroupModel> poolGroupList = new List<PoolGroupModel>();

  PoolOverviewRoutePageComponent(this._cs, this._ss, this.routes) {
    new Timer.periodic(const Duration(seconds: 1), (_) => _lastUpdatedDuration = getLastUpdatedDuration());
  }

  Future<Null> ngOnInit() async {
    poolGroupList = new List<PoolGroupModel>();
    Config config = await _cs.config;
    config.pools.forEach((Pool p) {
      Coin c = config.getCoinByCode(p.coincode);
      Algo a = config.getAlgoById(c.algoid);
      PoolGroupModel pgm = new PoolGroupModel(_ss, p, c, a);
      poolGroupList.add(pgm);
    });
    isLoaded = true;
  }

  Duration getLastUpdatedDuration() {
    DateTime last = _ss.lastUpdated;
    if(null == last) {
      return _lastUpdatedDuration = null;
    }
    DateTime current = new DateTime.now();
    return current.difference(last);
  }
}
