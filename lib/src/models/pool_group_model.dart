import 'package:chainaxe_website/model.dart';
import 'package:chainaxe_website/src/services/stats_service.dart';

class PoolGroupModel {
  final StatsService _ss;
  final Pool pool;
  final Coin coin;
  final Algo algo;

  PoolStats get poolStats {
    try {
      return _ss.data.getPoolStatsByPoolId(pool.id);
    } catch (e, t) {
      return null;
    }
  }

  PoolGroupModel(this._ss, this.pool, this.coin, this.algo);
}
