// ignore_for_file: uri_has_not_been_generated
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'package:chainaxe_website/src/components/route_page_components/pool_overview_route_page/pool_overview_route_page_component.template.dart'
    as porpc;
import 'package:chainaxe_website/src/components/route_page_components/index_route_page/index_route_page_component.template.dart'
    as irpc;
import 'package:chainaxe_website/src/components/route_page_components/pool_id_route_page/pool_id_route_page.template.dart'
    as pirpc;

@Injectable()
class AppRoutesService {

  final homeRoute = new RouteDefinition(
    path: '/home',
    component: irpc.IndexRoutePageComponentNgFactory,
    useAsDefault: true,
  );
  final poolRoute = new RouteDefinition(
    path: '/pool',
    component: porpc.PoolOverviewRoutePageComponentNgFactory,
  );
  final poolIdRoute = new RouteDefinition(
    path: '/pool/:id',
    component: pirpc.PoolIdRoutePageComponentNgFactory,
  );
  final poolIdAccountRoute = new RouteDefinition(
    path: '/pool/:id/:account',
    component: pirpc.PoolIdRoutePageComponentNgFactory,
  );
  final defaultRoute = new RouteDefinition.redirect(path: '', redirectTo: '/home');

  List<RouteDefinition> all;

  AppRoutesService() {
    all = [
      homeRoute,
      poolRoute,
      poolIdRoute,
      defaultRoute,
    ];
  }

}
