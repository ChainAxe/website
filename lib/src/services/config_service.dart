import 'dart:async';
import 'dart:convert';

import 'package:angular/angular.dart';
import 'package:async/async.dart';
import 'package:http/browser_client.dart';
import 'package:http/http.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:chainaxe_website/model.dart';
import 'package:chainaxe_website/serializer.dart';

@Injectable()
class ConfigService {
  final standardSerializers =
      (serializers.toBuilder()..addPlugin(new StandardJsonPlugin())).build();

  final AsyncCache<Config> _config =
      new AsyncCache<Config>(const Duration(hours: 1));
  Future<Config> get config async {
    return _config.fetch(() async => await loadConfig());
  }

  ConfigService();

  Future<Config> loadConfig() async {
    Response response =
        await new BrowserClient().get('https://alpha.chainaxe.io/api/client_config');
    final jsond = json.decode(response.body);
    return standardSerializers.deserializeWith(Config.serializer, jsond);
  }
}
