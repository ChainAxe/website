import 'dart:async';
import 'dart:convert';

import 'package:angular/angular.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:chainaxe_website/serializer.dart';
import 'package:chainaxe_website/model.dart';
import 'package:http/browser_client.dart';
import 'package:http/http.dart';

@Injectable()
class StatsService {
  final standardSerializers =
      (serializers.toBuilder()..addPlugin(new StandardJsonPlugin())).build();

  BasicStats _data;
  BasicStats get data => _data;

  Timer _t;
  DateTime _lastUpdated;

  DateTime get lastUpdated => _lastUpdated;

  static const maxErrors = 5;
  int errorCount = 0;

  StatsService() {
    triggerTimer();
  }

  startTimer() {
    cancelTimer();
    _t = new Timer.periodic(const Duration(seconds: 10), (_) => loadData());
  }

  triggerTimer() async {
    cancelTimer();
    await loadData();
    startTimer();
  }

  cancelTimer() {
    if (null != _t && _t.isActive) _t.cancel();
  }

  loadData() async {
    Response response =
        await new BrowserClient().get('https://alpha.chainaxe.io/api/basic_stats');
    if (response.statusCode == 200) {
      final jsond = json.decode(response.body);
      _data = standardSerializers.deserializeWith(BasicStats.serializer, jsond);
      _lastUpdated = new DateTime.now();
    } else {
      errorCount++;
      cancelTimer();
      if (errorCount >= maxErrors) {
        print("Max data load errors, canceling");
        errorCount = 0;
        startTimer();
      } else {
        await new Future.delayed(const Duration(milliseconds: 500));
        triggerTimer();
      }
    }
  }
}
