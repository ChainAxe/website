import 'dart:math';

import 'package:angular/angular.dart';

@Pipe('hashrate')
class HashratePipe extends PipeTransform {
  String transform(num hashrate, String hashUnit, [String nullText = "null"]) {
    if (null == hashrate) return nullText;
    return formatHashRate(hashrate, hashUnit);
  }

  static Map<String, int> hashrates = {
    "P": pow(10, 15),
    "T": pow(10, 12),
    "G": pow(10, 9),
    "M": pow(10, 6),
    "K": pow(10, 3)
  };

  static String formatHashRate(num hashrate, String hashUnit) {
    String suffix = "";
    hashrates.forEach((String character, int rate) {
      if (hashrate >= rate) {
        hashrate /= rate;
        suffix = character;
      }
    });
    return "${hashrate.toStringAsFixed(2)} ${suffix}${hashUnit}/s";
  }
}
