
import 'package:angular/angular.dart';

@Pipe('duration')
class DurationPipe extends PipeTransform {
  String transform(Duration duration, [String nullText = "Never"]) {
    if(null == duration) return nullText;
    int seconds = duration.inSeconds;
    if(seconds < 60) return "$seconds Seconds";
    int minutes = duration.inMinutes;
    if(minutes < 60) return "$minutes Minutes";
    int hours = duration.inHours;
    if(hours < 24) return "$hours Hours";
    int days = duration.inDays;
    return "$days Days";
  }
}
