import 'dart:math';

import 'package:angular/angular.dart';

@Pipe('difficulty')
class DifficultyPipe extends PipeTransform {
  static Map<String, int> diffs = {
    "P": pow(10, 15),
    "T": pow(10, 12),
    "G": pow(10, 9),
    "M": pow(10, 6),
    "K": pow(10, 3)
  };

  String transform(num difficulty, [String nullText = "null"]) {
    if (null == difficulty) return nullText;
    return formatDifficulty(difficulty);
  }

  static String formatDifficulty(num difficulty) {
    String suffix = "";
    diffs.forEach((String character, int diff) {
      if (difficulty >= diff) {
        difficulty /= diff;
        suffix = character;
      }
    });
    return "${difficulty.toStringAsFixed(2)} $suffix";
  }
}
