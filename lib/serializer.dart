library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'model.dart';

part 'serializer.g.dart';

@SerializersFor(const [
  Config,
  Algo,
  Coin,
  Pool,
  BasicStats,
  PoolStats,
  Miner,
])
final Serializers serializers = _$serializers;
