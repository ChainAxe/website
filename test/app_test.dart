@TestOn('browser')

import 'package:angular_test/angular_test.dart';
import 'package:chainaxe_website/app_component.dart';
import 'package:test/test.dart';

import 'app_test.template.dart' as ng;

void main() {
  ng.initReflector();
  final testBed = new NgTestBed<AppComponent>();
  NgTestFixture<AppComponent> fixture;

  setUp(() async {
    fixture = await testBed.create();
  });

  tearDown(disposeAnyRunningTest);

  // Testing info: https://webdev.dartlang.org/angular/guide/testing

  test('Default greeting', () {
    expect(fixture.text, 'Hello Angular');
  });
}
