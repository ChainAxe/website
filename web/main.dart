import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular/experimental.dart';
import 'package:angular_router/angular_router.dart';
import 'package:pwa/client.dart' as pwa;

import 'package:chainaxe_website/app_component.template.dart' as app;

// ignore: uri_has_not_been_generated
import 'main.template.dart' as ng;

@GenerateInjector(const [
  routerProvidersHash,
])
final InjectorFactory chainaxeWebsiteApp = ng.chainaxeWebsiteApp$Injector;

Future<Null> main() async {
  new pwa.Client();
  bootstrapFactory(app.AppComponentNgFactory, chainaxeWebsiteApp);
}
